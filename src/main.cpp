#include <QtCore/QScopedPointer>
#include <QtWidgets/QApplication>

#include "pflp.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    QScopedPointer<Pflp> dialog(new Pflp);

    dialog->show();
    return app.exec();
}  // main
