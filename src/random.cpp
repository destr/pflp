#include "random.h"


Random::Random() {

}

double Random::nextDouble() {
    return dist_next_double_(generator_);
}

double Random::nextDouble(double max) {
    std::uniform_real_distribution<> dist(0, max);
    return dist(generator_);
}

int Random::nextInt() {
    return dist_next_int_(generator_);
}

int Random::nextInt(int max) {
    std::uniform_int_distribution<> dist(0, max - 1);
    return dist(generator_);
}

