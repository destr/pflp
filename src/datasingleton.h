#pragma once

#include <QtCore/QSharedPointer>

#include "instance.h"
#include "random.h"
#include "semaphore.h"
#include "solution.h"


class Data {
    friend class DataSingleton;
public:
    Instance instance;
    SolutionPtr solution;
    Random random;

private:
    Data();
    ~Data();
};

class DataSingleton {
public:

    static Data* instance();

private:
    DataSingleton();
};
