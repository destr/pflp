#include <QtGui/QFontMetrics>

#include "pointfeature.h"

PointFeature::PointFeature() {
    x = 0;
    y = 0;
    width_ = 0;
    height = 0;
    priority = 0;
    text = "";
    font = DEFAULT_FONT;
    fontsize = DEFAULT_FONT_SIZE;
}

PointFeature::PointFeature(const PointFeature& p){
    x        = p.x;
    y        = p.y;
    width_   = p.width_;
    height   = p.height;
    priority = p.priority;
    text     = p.text;
    font     = p.font;
    fontsize = p.fontsize;
}

PointFeature::PointFeature(double _x, double _y, double lbl_size_x, double lbl_size_y, double p, QString _text, QString _font, int _fontsize)
{
    x = _x;
    y = _y;
    width_ = lbl_size_x;
    height = lbl_size_y;
    priority = p;
    text = _text;
    font = _font;
    fontsize = _fontsize;
}

PointFeature::PointFeature(double _x, double _y, double p, QString _text)
{
    x = _x;
    y = _y;
    priority = p;
    text = _text;
    font = DEFAULT_FONT;
    fontsize = DEFAULT_FONT_SIZE;

    calcLabelSize();
}

PointFeature::PointFeature(double _x, double _y, double p, QString _text, QString _font, int _fontsize)
{
    x = _x;
    y = _y;
    priority = p;
    text = _text;
    font = _font;
    fontsize = _fontsize;

    calcLabelSize();
}

void PointFeature::calcLabelSize() {
    QFontMetrics fm(QFont(font, fontsize));

    QRect r = fm.boundingRect(text);
    width_ = r.width();
    height = r.height();

}

bool PointFeature::canIntersect(PointFeature node) const
{
    return ((getWidth() + node.getWidth() >= qAbs(getX() - node.getX()))
                && (getHeight() + node.getHeight() >= qAbs(getY() - node.getY())));
}

QString PointFeature::getFont() const
{
    return font;
}

int PointFeature::getFontsize() const
{
    return fontsize;
}

double PointFeature::getHeight() const
{
    return height;
}

double PointFeature::getWidth() const
{
    return width_;
}

double PointFeature::getPriority() const
{
    return priority;
}

QString PointFeature::getText() const
{
    return text;
}

double PointFeature::getX() const
{
    return x;
}

double PointFeature::getY() const
{
    return y;
}

void PointFeature::setX(double d) {
    x = d;
}

void PointFeature::setY(double d) {
    y = d;
}

