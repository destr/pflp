#include "datasingleton.h"

Data* DataSingleton::instance() {
    static Data data;
    return &data;
}  // instance

Data::Data() {
    solution.reset(new Solution(instance, false));
}

Data::~Data()
{

}
