#pragma once

#include <QtWidgets/QDialog>

#include "search/baselabeling.h"

namespace Ui {
class Pflp;
}

typedef QVector<BaseLabeling*> AlgorithmList;
class Pflp : public QDialog
{
    Q_OBJECT

public:
    explicit Pflp(QWidget *parent = 0);
    ~Pflp();

private slots:
    void on_runClicked();

private:
    AlgorithmList algorithm_list_;
    Ui::Pflp *ui;
};
