#include <QtCore/QDebug>

#include "datasingleton.h"
#include "pflp.h"
#include "search/forcedirectedlabeling.h"
#include "search/hirschlabeling.h"
#include "ui_pflp.h"

namespace {

LabelPtrList& CopyList(const LabelPtrList &ptrlist, LabelPtrList &list) {
    list.clear();
    for (const Label *l : ptrlist) {
        list.push_back((new Label(*l)));
    }
    return list;
}
}

Pflp::Pflp(QWidget *parent) : QDialog(parent), ui(new Ui::Pflp) {
    ui->setupUi(this);

    connect(ui->pushButtonRun, &QPushButton::clicked, this, &Pflp::on_runClicked);

    algorithm_list_.push_back(new ForceDirectedLabeling);
    algorithm_list_.push_back(new HirschLabeling);

    for (const BaseLabeling *t : algorithm_list_) {
        ui->comboBoxAlgorithm->addItem(t->getAlgorithmName());
    }

}  // Ctor

Pflp::~Pflp() {
    qDeleteAll(algorithm_list_);
    delete ui;
}  // Dtor

void Pflp::on_runClicked() {
    LabelPtrList list;
    SolutionPtr s = DataSingleton::instance()->solution;

    ui->widgetLeft->setLabelList(CopyList(s->getLabels(), list));
    ui->widgetLeft->setDrawEnabled(true);

    int index = ui->comboBoxAlgorithm->currentIndex();
    if (index == -1) return;
    BaseLabeling *algo = algorithm_list_[index];
    algo->run();

    ui->widgetRight->setDrawEnabled(true);
    ui->widgetRight->setLabelList(CopyList(s->getLabels(), list));
    update();

}  // on_runClicked
