#pragma once

#include <QtCore/QObject>
#include <QtCore/QVector>

/**
 * This class represents a city with it's location and the 
 * required space for it's corresponding label.
 * The corresponding coordinates are normalized to be 
 * greater than 0 in each component
 *
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */
class PointFeature {
public:
    QString DEFAULT_FONT = "Arial";
    int DEFAULT_FONT_SIZE = 16;

private:
	/**
	 * the x coordinate of the city
	 */
    double x;
	/**
	 * the x coordinate of the city
	 */
    double y;
	/**
	 * the width of the corresponding label
	 */
    double width_;
	/**
	 * the height of the corresponding label
	 */
    double height;
	/**
	 * the priority of this city (not yet used)
	 */
    double priority;
	/**
	 * the name of this city
	 */
    QString text;
	/**
	 * the font used to display the corresponding label
	 */
    QString font;
	/**
	 * the font size used to display the corresponding label
	 */
    int fontsize;

public:
    PointFeature();
    PointFeature(const PointFeature &p);

	/**
	 * constructs a new node with the given size and width
	 */
    PointFeature(
        double _x,
        double _y,
        double lbl_size_x,
        double lbl_size_y,
        double p,
        QString _text,
        QString _font,
        int _fontsize);


	/**
	 * constructs a new node. width and height will be determined by the needs of the text (default value)
	 */
    PointFeature(double _x, double _y, double p, QString _text);

	/**
	 * constructs a new node. width and height will be determined by the needs of the given text
	 */
    PointFeature(double _x, double _y, double p, QString _text, QString _font, int _fontsize);

    void calcLabelSize();

	/**
	 * returns true if and only if the label can intersect in any location with the
	 * label of the given node "node"
	 * @param node the node to compare
	 */
    bool canIntersect(PointFeature node) const;

	/**
	 * @return name of the font for the associated label
	 */
    QString getFont() const;

	/**
	 * @return size of the font for the associated label
	 */
    int getFontsize() const;

	/**
	 * @return Height of the label
	 */
    double getHeight() const;

	/**
	 * @return Width of the label
	 */
    double getWidth() const;

	/**
	 * @return Priority of the label
	 */
    double getPriority() const;

	/**
	 * @return Text for the label
	 */
    QString getText() const;

	/**
	 * @return x-coodinate of the label
	 */
    double getX() const;

	/**
	 * @return y-coodinate of the label
	 */
    double getY() const;

	/**
	 * @param d new x-coordinate
	 */
    void setX(double d);

	/**
	 * @param d new y-coordinate
	 */
    void setY(double d);
};

///
/// typedef PointFeature list
///
typedef QVector<PointFeature> PointFeatureList;
