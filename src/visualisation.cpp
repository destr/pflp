#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>

#include "datasingleton.h"
#include "solution.h"
#include "visualisation.h"

Visualisation::Visualisation(QWidget *parent) : QWidget(parent),
    draw_enabled_(false)
{

}

Visualisation::~Visualisation() {
    qDeleteAll(list_);
}

void Visualisation::setDrawEnabled(bool enabled) {
    draw_enabled_ = enabled;
}

void Visualisation::setLabelList(const LabelPtrList& list){
    list_ = list;
}

void Visualisation::paintEvent(QPaintEvent *event) {
    QWidget::paintEvent(event);

    if (!draw_enabled_) return;

    QPainter painter(this);

    painter.setPen(QPen(QBrush(Qt::white), 3));

    for (const Label *l : list_) {
        painter.drawPoint(l->getTopleft());
        painter.drawText(l->getTopleft(), l->getNode().getText());
    }
}
