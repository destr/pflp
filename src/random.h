#pragma once

#include <QTCore/qglobal.h>

#include <random>

class Random {

public:

    Random();

    double nextDouble();
    double nextDouble(double max);
    int nextInt();
    int nextInt(int max);


private:
    std::default_random_engine generator_;
    std::uniform_int_distribution<> dist_next_int_;
    std::uniform_real_distribution<> dist_next_double_;

private:
    Q_DISABLE_COPY(Random)
};
