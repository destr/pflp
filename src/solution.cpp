#include "solution.h"


Solution::Solution(Instance inst) : Solution(inst, true) {
}

Solution::~Solution() {
    qDeleteAll(labels);
}

Solution::Solution(Instance inst, bool init_solution) {
    instance = inst;

    labels.resize(instance.getNodes().size());

    for (int i = 0; i < instance.getNodes().size(); i++)
        labels[i] = new Label(instance.getNodes()[i], i);

    setNeighbours();

    if(init_solution)
        findInitialPlacement();
}

void Solution::setNeighbours() {
    for (int i = 0; i < labels.size(); i++) {
        Label *current = labels[i];

        //create a list of neighbours
        for (int j = 0; j < labels.size(); j++) {
            if (i != j) {
                if (current->getNode().canIntersect(labels[j]->getNode()))
                    current->addNeighbour(labels[j]);
            }
        }
    }
}

void Solution::findInitialPlacement() {
    for (int i = 0; i < labels.size(); i++) {
        if (labels[i]->hasNeighbours())
            labels[i]->findInitialPlacement();
        else
            labels[i]->moveTo(0., 0.);
    }
}

const LabelPtrList&Solution::getLabels() const {
    return labels;
}

int Solution::countLabeledCities() const {
    int set = 0;
    for (int i = 0; i < labels.size(); i++) {
        if (!labels[i]->getUnplacable() && !labels[i]->isOverlapping())
            set++;
    }

    return set;
}

int Solution::countUnlabeledCities() const {
    return (labels.size() - countLabeledCities());
}

bool Solution::existsOverlapping() const {
    for(int k = 0; k < labels.size(); k++) {
        if(labels[k]->isOverlapping())
            return true;
    }
    return false;
}

int Solution::size() const {
    return instance.size();
}

const Instance&Solution::getInstance() const {
    return instance;
}

