#pragma once

#include <random>
#include <cstring>

#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QtMath>

#include "pointfeature.h"

/**
 * This class represents a instance for the map labeling problem.
 * It holds an collection of all points and the given map height and
 * width. 
 *
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */
class Instance
{
    static const int RANDOM_MAP_WIDTH = 100;
    static const int RANDOM_MAP_HEIGHT = 80;
    static const int RANDOM_MAP_LABELS = 100;
    static const int RANDOM_MAP_SCALE = 6;

    PointFeatureList nodes;

    int map_width = 0;
    int map_height = 0;

    QString name = "<not yet set>";

public:
	/**
	 * creates an instance with the nodes given in Vector v
	 */
    Instance(const PointFeatureList &v, QString solution_name);

	/**
	 * creates an instance with random distributed nodes. 
	 */
    Instance();


    void adjust_coordinates();
	/**
	 * returns the width of the instance (map)
	 */
    int getMapWidth() const;

	/**
	 * returns the height of the instance (map)
	 */
    int getMapHeight() const;

	/**
	 * returns a reference to the array of nodes
	 */
    const PointFeatureList& getNodes() const;

	/**
	 * returns the name of the file or the string "random instance"
	 */
    const QString& getName() const;

	/**
	 * @param string the new name of the instance
	 */
    void setName(QString string);

	/**
	 * @see hugs.Problem#size()
	 */
    int size() const;
};
