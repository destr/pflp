#pragma once

#include <QtCore/QSharedPointer>

#include "label.h"
#include "instance.h"

/**
 * Represents a solution of the map labeling problems
 * (a mapping of the label to (x, y) - coordinates).
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */

class Solution {
public:

    Solution(Instance inst);
    ~Solution();

	/**
	 * constructs a new initial Solution to the given instance.
	 * For every node a list of neighbours will be generated.
	 * The initial position is one of the four possible corner positions 
	 * @param inst a reference to the {@link Instance instance object} 
	 */
    Solution(Instance inst, bool init_solution);

    void setNeighbours();
    void findInitialPlacement();

	/**
	 * returns the array of all {@link LblLabel labels} corresponding to the given {@link Instance instance}
	 */
    const LabelPtrList &getLabels() const;

	/**
	 * counts the number of valid labeled cities (no intersections).
	 */
    int countLabeledCities() const;

	/**
	 * counts the number of intersecting or unused labels.
	 */
    int countUnlabeledCities() const;

	/**
	 * 
	 * @return true <-> at least one label is obstructed by another one
	 */
    bool existsOverlapping() const;

	/** returns the size of the solution
	 * @see hugs.Solution#size()
	 */
    int size() const;

	/**
	 * @return reference to the associated instance
	 */
    const Instance& getInstance() const;

private:
    //the instance for this solution
    Instance instance;

    //where to place the labels
    LabelPtrList labels;


};
/// \brief Умный укаталь на объект
typedef QSharedPointer<Solution> SolutionPtr;
