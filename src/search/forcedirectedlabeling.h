#pragma once

#include <QtCore/qglobal.h>
#include <QtCore/QPointF>
#include <QtCore/QRectF>

#include "baselabeling.h"

/**
 * force directed labeling
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */

class ForceDirectedLabeling: public BaseLabeling {
public:
    struct Coord {
        double x;
        double y;
        Coord();
        Coord& operator +=(const Coord &c);
        Coord& operator -=(const Coord &c);
        Coord& operator-();
    };

public:
    ForceDirectedLabeling();
    void enableSimpleCleanup();

protected:
    void precompute() override;

    bool iterate() override;
private:

    /**
     * moves the label to a randomly chosen position (4pos - model)
     * forces on the label and/or neighbours are not updated!
     * @param index the label to be moved...
     */
    void toRandomPosition(int index, bool updateForce);

    /**
     * returns a random label from the set of obstructed labels
     * or null, if such a label does not exist
     */
    Label* chooseNextCandidate();
    bool canSlideHorizontal(const Label* l) const;

    bool canSlideVertical(const Label* lp) const;

    static int signum(double d);

    void findEquilibrium(const Label* current, bool slide_h);

    /**
     * moves the label value units in the direction determined by
     * the first parameter
     * be careful to update the force if required
     * @param l the label to be moved
     * @param direction horizontal or vertical moves?
     * @param value determines amount of the change
     */
    void slideBy(const Label *l, int direction, double value);

    void removeLabel(int i);

    void moveLabel(int i, double h_offset, double v_offset);

    bool existsOverlapping() const;

    /**
     * Returns true, if the l1 intersects with the given
     * label l2. This means, there is at least on point covered by
     * both labels.
     */
    bool doesIntersect(const Label* l1, const Label* l2) const;

    double calcForceValue(const Coord& f);

    /**
     * calculates the resultingforce vector according to the specific force model
     */
    void updateForce(int i);

    Coord computeForce(int i_from, int i_to);

    QString getLabelInfo(int i);
private:
    static constexpr double DEFAULT_FORCE_FAKT_REPULSIVE = 1.0;
    static constexpr double DEFAULT_FORCE_FAKT_OVERLAPPING = 10.0;
    static constexpr double DEFAULT_FORCE_FAKT_EPS = 0.5;
    static constexpr double DEFAULT_OVERLAPPING_PENALTY = 3 * (1 / (DEFAULT_FORCE_FAKT_EPS * DEFAULT_FORCE_FAKT_EPS));

    /**
      * if the resulting force (absolute value) is smaller than this value it is
      * handled like it would be zero.
      */
    static constexpr  double MIN_FORCE = 0.5;

    static const int SLIDE_HORIZONTAL = 1;
    static const int SLIDE_VERTICAL = 2;

    double temperature ;
    double cooling_rate ;
    int    moves_per_stage;
    int    size;

    long   nRejected;
    long   nTaken;
    long   nUnsignificant;

    bool simpleCleanup;
    double overallForce;

    LabelPtrList labels;

    // 32 МБ
    static const int max_labels = 2000;
    typedef std::array<Coord, max_labels> LabelForces;
    typedef std::array<LabelForces, max_labels> LabelLabelForces;

    LabelForces label_forces;
    LabelLabelForces label_label_forces;

    QSet<Label*> obstructed;

private:
    Q_DISABLE_COPY(ForceDirectedLabeling)
};
