#pragma once

#include <QtCore/qglobal.h>
#include <QtCore/QString>

#include "datasingleton.h"
#include "solution.h"

/**
 * The thread implementing the algorithm.
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */

class BaseLabeling {
public:
    BaseLabeling();
    virtual ~BaseLabeling();
    void run();

    /**
     * @return name of the algorithm
     */
    QString getAlgorithmName() const;

	/**
	 * whenever the visualization class creates a copy of the current solution it will
	 * call this function to retrieve some debug information about the current algorithm
	 * that can be displayed instead of label names.
	 */
    QString getLabelInfo(int i) const;
    /**
     * subsequently removes the label with the maximum
     * number of intersections, until no more intersections
     * are detected
     */
    void cleanupSolution(SolutionPtr solution);
    void cleanupSolutionSimple(SolutionPtr solution);
	
protected:
	/**
	 * does the real work
	 * @return true <-> algorithm is ready
	 */
    virtual bool iterate() = 0;

	/**
	 * computations that should happen only once
	 */
    virtual void precompute() = 0;

protected:
    QString name;
private:
    bool halt;

    Q_DISABLE_COPY(BaseLabeling)
};
