#include "forcedirectedlabeling.h"

ForceDirectedLabeling::ForceDirectedLabeling() :
    temperature(0),
    cooling_rate(0),
    moves_per_stage(0),
    size(0),
    nRejected(0),
    nTaken(0),
    nUnsignificant(0),
    simpleCleanup(false),
    overallForce(0.0)   {
    name = QString("force directed labeling");
}

void ForceDirectedLabeling::enableSimpleCleanup() {
    simpleCleanup = true;
}

void ForceDirectedLabeling::precompute() {
    int i;

    //create initial solution
    Data *d = DataSingleton::instance();

    labels = d->solution->getLabels();

    size = d->solution->size();

    Q_ASSERT(size < max_labels);

    //label_forces = LabelForces(size);

    //label_label_forces = LabelLabelForces(size, LabelForces(size));

    obstructed.reserve(size);

    overallForce = 0.0;

    //find good start position
    for (i = 0; i < size; i++)
        toRandomPosition(i, false);

    //initialize forces...
    for (i = 0; i < size; i++)
        updateForce(i);

    //init temperature && initialize set of obstructed labels...
    double avg_lbl_size = 0;
    for (i = 0; i < size; i++)
    {
        if(!labels[i]->getUnplacable() && (labels[i]->isOverlapping() || canSlideHorizontal(labels[i]) || canSlideVertical(labels[i])))
            obstructed.insert(labels[i]);

        avg_lbl_size += labels[i]->getHeight() * labels[i]->getWidth();
    }
    avg_lbl_size /= size;

    //we accept a overlap of p2 of the average label size with p1
    double p1 = 0.3; //propability of acceptance
    double p2 = 0.5; //percentage of overlap
    double eps_2 = DEFAULT_FORCE_FAKT_EPS * DEFAULT_FORCE_FAKT_EPS;
    temperature = avg_lbl_size * p2 * DEFAULT_FORCE_FAKT_OVERLAPPING + DEFAULT_OVERLAPPING_PENALTY + DEFAULT_FORCE_FAKT_REPULSIVE / eps_2;
    temperature /= -qLn(p1);

    //temp. should become < 1 after N stages...
    double N=15;
    cooling_rate = qPow(1. / temperature, 1. / N);

    //moves per stage...
    moves_per_stage = 30 * size;

    nRejected = 0;
    nTaken = 0;
    nUnsignificant = 0;
}

bool ForceDirectedLabeling::iterate() {
    Label *current = chooseNextCandidate();

    //are there any movable or overlapping labels left?
    if(!current) {
        return true;
    }

    //save some required label infos
    int    current_index = current->getIndex();
    double old_force = overallForce;
    double old_v_offset = current->getOffsetVertical();
    double old_h_offset = current->getOffsetHorizontal();

    //if the label want's to slide somewhere -> do it...
    bool slideable_h = canSlideHorizontal(current);
    bool slideable_v = canSlideVertical(current);

    //bool removeObstructed = false;
    if(slideable_h || slideable_v) {
        bool slide_h = false;
        if(slideable_h && slideable_v) //flip a coin...
            slide_h = DataSingleton::instance()->random.nextDouble() <= (qAbs(label_forces[current_index].x) / (qAbs(label_forces[current_index].x) + qAbs(label_forces[current_index].y)));
        else
            slide_h = slideable_h;

        findEquilibrium(current, slide_h);

        if((slide_h && canSlideHorizontal(current)) || (!slide_h && canSlideVertical(current)))
            toRandomPosition(current_index, true);
    } else { //the label intersects at least one other label -> random position
        toRandomPosition(current_index, true);
    }

    //take the move?
    double dE = overallForce - old_force;
    double p = DataSingleton::instance()->random.nextDouble();

    if (dE > 0.0 && p > qExp(-dE / temperature)) {
        //reject move
        moveLabel(current_index, old_h_offset, old_v_offset);
        nRejected ++;
    } else {
        //update set of obstructed labels....
        if(!current->isOverlapping() && !canSlideHorizontal(current) && !canSlideVertical(current))
            obstructed.remove(current);

        LabelPtrListIterator ni(current->getNeighbours());
        while (ni.hasNext()) {
            Label *ln = ni.next();
            if(ln->isOverlapping() || canSlideHorizontal(ln) || canSlideVertical(ln))
                obstructed.insert(ln);
            else
                obstructed.remove(ln);
        }

        nTaken ++;
        if(qAbs(dE) < MIN_FORCE)
            nUnsignificant ++;
    }

    if (nTaken + nRejected >= moves_per_stage) {
        int max_ovl = 0;
        Label *candidate(nullptr);

        QSetIterator<Label*> d(obstructed);
        while(d.hasNext()) {
            Label *l = d.next();
            int n = 0;

            LabelPtrListIterator it(l->getNeighbours());
            while(it.hasNext()) {
                Label *l2 = it.next();
                if(!l2->getUnplacable() && l->doesIntersect(l2))
                    n ++;
            }

            if(n > max_ovl) {
                max_ovl = n;
                candidate = l;
            }
        }

        if(!candidate) {
            return true;
        }


        if(nTaken - nUnsignificant <= 0) {

            if(simpleCleanup) {
                cleanupSolution(DataSingleton::instance()->solution);
                return true;
            }


            removeLabel(candidate->getIndex());
        }

        //decrease temperature
        temperature = temperature * cooling_rate;

        //adjust moves_per_stage
        moves_per_stage = qMax(size, qMin(50 * obstructed.size(), 10 * size));

        nRejected = 0;
        nTaken = 0;
        nUnsignificant = 0;
    }

    return false; //not yet ready...
}

void ForceDirectedLabeling::toRandomPosition(int index, bool updateForce)
{
    double old_h = labels[index]->getOffsetHorizontal();
    double old_v = labels[index]->getOffsetVertical();

    do
    {
        int npos = DataSingleton::instance()->random.nextInt(8);
        switch(npos)
        {
            case 0:
                labels[index]->moveTo(Label::TOPLEFT);
                break;
            case 1:
                labels[index]->moveTo(Label::TOPRIGHT);
                break;
            case 2:
                labels[index]->moveTo(Label::BOTTOMLEFT);
                break;
            case 3:
                labels[index]->moveTo(Label::BOTTOMRIGHT);
                break;
            case 4:
                labels[index]->moveTo(labels[index]->getWidth() / 2., labels[index]->getHeight());
                break;
            case 5:
                labels[index]->moveTo(0, labels[index]->getHeight() / 2.);
                break;
            case 6:
                labels[index]->moveTo(labels[index]->getWidth() / 2., 0);
                break;
            case 7:
                labels[index]->moveTo(labels[index]->getWidth(), labels[index]->getHeight() / 2.);
                break;
        }
    }while(old_h != labels[index]->getOffsetHorizontal() && old_v == labels[index]->getOffsetVertical());

    if(updateForce)
        this->updateForce(index);
}

Label* ForceDirectedLabeling::chooseNextCandidate() {
    if(!obstructed.isEmpty()) {
        int i = DataSingleton::instance()->random.nextInt(obstructed.size());
        return *(obstructed.begin() + i);
    }

    return nullptr;
}

bool ForceDirectedLabeling::canSlideHorizontal(const Label *l) const
{
    return (
                (l->getOffsetVertical() == 0.0 || l->getOffsetVertical() == l->getHeight())
                && qAbs(label_forces[l->getIndex()].x) >= MIN_FORCE
            && ((label_forces[l->getIndex()].x > 0.0 && l->getOffsetHorizontal() > 0)
            || (label_forces[l->getIndex()].x < 0.0 && l->getOffsetHorizontal() < l->getWidth())));

}

bool ForceDirectedLabeling::canSlideVertical(const Label* lp) const {
    const Label &l = *lp;
    return (
                (l.getOffsetHorizontal() == 0.0 || l.getOffsetHorizontal() == l.getWidth())
                && qAbs(label_forces[l.getIndex()].y) >= MIN_FORCE
            && ((label_forces[l.getIndex()].y > 0.0 && l.getOffsetVertical() > 0.0)
            || (label_forces[l.getIndex()].y < 0.0 && l.getOffsetVertical() < l.getHeight())));

}

int ForceDirectedLabeling::signum(double d)
{
    return d >= 0. ? 1 : -1;
}

void ForceDirectedLabeling::findEquilibrium(const Label *current, bool slide_h)
{
    int slide_dir = slide_h ? SLIDE_HORIZONTAL : SLIDE_VERTICAL;
    int i_current = current->getIndex();
    int searchIterations = 0;

    int old_direction = signum(slide_h ? label_forces[i_current].x : label_forces[i_current].y);

    double total = 0;
    if(old_direction == 1)
        total = slide_h ? current->getOffsetHorizontal() : current->getOffsetVertical();
    else
        if(slide_h)
            total = current->getWidth() - current->getOffsetHorizontal();
        else
            total = current->getHeight() - current->getOffsetVertical();

    double amount = total * 0.2; //% of total (remaining) width/height

    double force_diff = 0;
    do
    {
        if((slide_h && !canSlideHorizontal(current)) || (!slide_h && !canSlideVertical(current)))
        {
            return;
        }

        force_diff = slide_h ? label_forces[i_current].x : label_forces[i_current].y;

        slideBy(current, slide_dir, amount);

        force_diff = qAbs(force_diff - (slide_h ? label_forces[i_current].x : label_forces[i_current].y));

        int new_direction = signum(slide_h ? label_forces[i_current].x : label_forces[i_current].y);
        if(old_direction != new_direction)
        {
            old_direction = new_direction;
            amount /= 2.;
        }

        searchIterations++;

    }while(searchIterations < 20 && qAbs(slide_h ? label_forces[i_current].x : label_forces[i_current].y) >= MIN_FORCE);
}

void ForceDirectedLabeling::slideBy(const Label* l, int direction, double value)
{
    double h_offset = l->getOffsetHorizontal();
    double v_offset = l->getOffsetVertical();
    int i_label = l->getIndex();

    value = qAbs(value);

    if (direction == SLIDE_HORIZONTAL) {
        if (label_forces[i_label].x > 0)
            h_offset = qMax(0.0, l->getOffsetHorizontal() - value);
        else
            h_offset = qMin(l->getWidth(), l->getOffsetHorizontal() + value);
    } else {
        if (label_forces[i_label].y > 0)
            v_offset = qMax(0.0, l->getOffsetVertical() - value);
        else
            v_offset = qMin(l->getHeight(), l->getOffsetVertical() + value);
    }

    moveLabel(i_label, h_offset, v_offset);
}

void ForceDirectedLabeling::removeLabel(int i) {
    labels[i]->setUnplacable(true);
    obstructed.remove(labels[i]);

    overallForce -= calcForceValue(label_forces[i]);

    label_forces[i].x = label_forces[i].y = 0.0; //reset force

    //repair the forces when removing a label
    LabelPtrListIterator it(labels[i]->getNeighbours());
    while (it.hasNext())
    {
        int j = it.next()->getIndex();

        //updated force(s)
        overallForce -= calcForceValue(label_forces[j]);

        label_forces[j] -= label_label_forces[j][i];

        label_label_forces[i][j].x = label_label_forces[j][i].x = 0.0;
        label_label_forces[i][j].y = label_label_forces[j][i].y = 0.0;


        overallForce += calcForceValue(label_forces[j]);

        //is the neighour still obstructed?
        Label *ln = labels[j];
        if(!ln->getUnplacable() && (ln->isOverlapping() || canSlideHorizontal(ln) || canSlideVertical(ln)))
            obstructed.insert(ln);
        else
            obstructed.remove(ln);
    }
    return;
}

void ForceDirectedLabeling::moveLabel(int i, double h_offset, double v_offset) {
    labels[i]->moveTo(h_offset, v_offset);
    updateForce(i);
}

bool ForceDirectedLabeling::existsOverlapping() const {
    for(int k = 0; k < size; ++k) {
        if(!labels[k]->getUnplacable() && labels[k]->isOverlapping())
            return true;
    }

    return false;
}

bool ForceDirectedLabeling::doesIntersect(const Label *l1, const Label *l2) const {
    const QPointF tl1 = l1->getTopleft();
    const QPointF tl2 = l2->getTopleft();

    return (
                (tl2.x() + l2->getWidth() > tl1.x() && tl2.x() < tl1.x() + l1->getWidth())
                && (tl2.y() + l2->getHeight() > tl1.y() && tl2.y() < tl1.y() + l1->getHeight()));
}

void ForceDirectedLabeling::updateForce(int i)
{
    if (labels[i]->getUnplacable())
        return;

    overallForce -= calcForceValue(label_forces[i]);

    label_forces[i].x = label_forces[i].y = 0.0; //reset force

    LabelPtrListIterator it(labels[i]->getNeighbours());
    while (it.hasNext())
    {
        Label *l2 = it.next();

        if (!l2->getUnplacable())
        {
            int j = l2->getIndex();
            Coord  f_n = computeForce(i, j);

            label_label_forces[i][j] = f_n;

            label_forces[i] += f_n;

            //repair force vector at the current neighbour...
            overallForce -= calcForceValue(label_forces[j]);

            label_forces[j] -= label_label_forces[j][i];

            //force(i,j) = -force(j,i)
            label_label_forces[j][i] = -f_n;

            label_forces[j] += label_label_forces[j][i];

            overallForce += calcForceValue(label_forces[j]);
        }
    }

    overallForce += calcForceValue(label_forces[i]);
}

QString ForceDirectedLabeling::getLabelInfo(int i)
{
    if (i >= 0 && i < size)
    {
        QString s;
        if (canSlideHorizontal(labels[i]))
        {
            s += label_forces[i].x > 0 ? "R" : "L";
            s += "(" + QString::number(((double)qRound(label_forces[i].x * 10)) / 10) + ")";
        }

        if (canSlideVertical(labels[i]))
        {
            s += label_forces[i].y > 0 ? "D" : "U";
            s += "(" + QString::number(((double)qRound(label_forces[i].y * 10)) / 10) + ")";
        }

        if(obstructed.contains(labels[i]))
            s = "[*]" + s;

        return s;
    }
    else
    {
        return QString();
    }
}


ForceDirectedLabeling::Coord ForceDirectedLabeling::computeForce(int i_from, int i_to)
{
    Label *l1 = labels[i_from];
    Label *l2 = labels[i_to];

    const double K1 = DEFAULT_FORCE_FAKT_REPULSIVE;
    const double K2 = DEFAULT_FORCE_FAKT_OVERLAPPING;
    const double eps = DEFAULT_FORCE_FAKT_EPS;

    double f = 0.0;
    double l = l1->getDistance(l2);

    //the part proportional to the distance between two labels (small)
    double v = qMax(l, eps);
    f = K1 / (v * v);

    //the part proportional to the intersection (if any) of the two labels
    if (l < 0)
    {
        QRectF r_int = l1->getRectangle().intersected(l2->getRectangle());
        if (!r_int.isEmpty())
        {
            double area = r_int.height() * r_int.width();
            f += K2 * area;
            f += DEFAULT_OVERLAPPING_PENALTY;
        }
    }

    QPointF p1 = l1->getCenter();
    QPointF p2 = l2->getCenter();
    double dx = p1.x() - p2.x();
    double dy = p1.y() - p2.y();
    double d = qSqrt(dx * dx + dy * dy);

    Coord f_ret;
    if (d != 0.0)
    {
        f_ret.x = f * dx / d;
        f_ret.y = f * dy / d;
    }
    return f_ret;
}

double ForceDirectedLabeling::calcForceValue(const Coord& f)
{
    return qSqrt(f.x * f.x + f.y * f.y);
}



ForceDirectedLabeling::Coord::Coord() : x(0.0), y(0.0)
{ }

ForceDirectedLabeling::Coord&ForceDirectedLabeling::Coord::operator +=(const ForceDirectedLabeling::Coord& c) {
    x += c.x;
    y += c.y;
    return *this;
}

ForceDirectedLabeling::Coord&ForceDirectedLabeling::Coord::operator -=(const ForceDirectedLabeling::Coord& c) {
    x -= c.x;
    y -= c.y;
    return *this;
}

ForceDirectedLabeling::Coord&ForceDirectedLabeling::Coord::operator-() {
    x = -x;
    y = -y;
    return *this;
}
