#pragma once

#include <QtCore/QRectF>

#include "baselabeling.h"



/**
 * hirsch's method
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */
class HirschLabeling : public BaseLabeling
{
public:
    static const int X = 0;
    static const int Y = 1;

private:
    static const int MAX_ITERATIONS = 1000;

    static const int SLIDE_HORIZONTAL = 1;
    static const int SLIDE_VERTICAL = 2;

    static const int METHOD1 = 1; //incremental movement
    static const int METHOD2 = 2; //absolut movement

    int size = 0;
    LabelPtrList labels;
    SolutionPtr solution;

    typedef std::array<double, 2> DoublePoint;
    typedef QVector<DoublePoint> DoublePointList;
    DoublePointList overlap_vectors ;
    int nInterations = 0;
public:
    HirschLabeling();

    QString getLabelInfo(int i);

    void precompute() override;

protected:
    bool iterate() override;

private:
    void mapSweep(int method);

    void computeOverlapVectors();

    void computeOverlapVector(int i);

    bool canSlideHorizontal(const Label *l) const;

    bool canSlideVertical(const Label* ptr) const;

	/**
	 * moves the label value units in the direction determined by 
	 * the first parameter
	 * @param l the label to be moved	 
	 * @param direction horizontal or vertical moves?
	 * @param value determines amount of the change
	 */
public:
    void slideBy(Label* ptr, int direction, double value);
};
