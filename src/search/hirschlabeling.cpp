#include "hirschlabeling.h"


HirschLabeling::HirschLabeling() {
    name = QString("hirsch");
}

QString HirschLabeling::getLabelInfo(int i)
{
    if (i >= 0 && i < size)
    {
        return QString("(, )")
                    .arg(((double) qRound(overlap_vectors[i][X] * 10)) / 10)
                     .arg(((double) qRound(overlap_vectors[i][Y] * 10)) / 10)
                    ;
    }
    return QString();
}

void HirschLabeling::precompute() {
    int i = 0;

    Data* d = DataSingleton::instance();

    labels = d->solution->getLabels();
    size = d->solution->size();
    solution = d->solution;


    overlap_vectors = DoublePointList(size, {0, 0});
    //init: special zone right from the point...
    for (i = 0; i < size; i++)
    {
        labels[i]->moveTo(0.0, labels[i]->getHeight());
    }

    nInterations = 0;

    //initialize overlap vectors...
    computeOverlapVectors();
}



bool HirschLabeling::iterate()
{

    if (!solution->existsOverlapping())
        return true;

    nInterations++;
    computeOverlapVectors();

    //apply method 2 every sixth iteration
    if (nInterations % 6 != 0)
        mapSweep(METHOD1);
    else
        mapSweep(METHOD2);

    return false;
}

void HirschLabeling::mapSweep(int method)
{
    for (int k = 0; k < size; k++)
    {
        Label *current = labels[k];
        if (method == METHOD1)
        {
            //like described in the paper by christensen/marks/shieber
            //the radius of the surrounding circle ist reduced to zero to make
            //the method comparable to other algorithms.

            //rule1 + rule2 is obsolete, since we reduced the size of the circle
            //to zero.
            //				double angle = Math.atan(overlap_vectors[k][Y] / overlap_vectors[k][X]);
            //				angle = angle * 180 / Math.PI;
            //				if(overlap_vectors[k][Y] < 0)
            //					angle += 180;

            bool slideable_v = canSlideVertical(current);
            bool slideable_h = canSlideHorizontal(current);

            if (!slideable_h && !slideable_v)
                continue;

            if ((slideable_h || slideable_v))
            {
                bool slide_h = false;
                if (slideable_h && slideable_v)
                {
                    if (qAbs(overlap_vectors[k][X]) > qAbs(overlap_vectors[k][Y]))
                        slide_h = true;
                    else
                        slide_h = false;
                }
                else if (slideable_h)
                {
                    slide_h = true;
                }
                else if (slideable_v)
                {
                    slide_h = false;
                }

                slideBy(
                            current,
                            slide_h ? SLIDE_HORIZONTAL : SLIDE_VERTICAL,
                            slide_h ? overlap_vectors[k][X] : overlap_vectors[k][Y]);
            }
        }
        else //METHOD 2 (absolut movement)
        {
            //implemented without using special zones. the method just moves
            //the label to the quadrant the vector indicates (4pos model)
            double h_offset = current->getOffsetHorizontal();
            double v_offset = current->getOffsetVertical();

            if(overlap_vectors[k][X] != 0.)
                h_offset = overlap_vectors[k][X] >= 0 ? 0.0 : current->getWidth();

            if(overlap_vectors[k][Y] != 0.)
                v_offset = overlap_vectors[k][Y] >= 0 ? 0.0 : current->getHeight();

            current->moveTo(h_offset, v_offset);
        }
    }
}

void HirschLabeling::computeOverlapVectors() {
    for (int i = 0; i < size; i++)
        computeOverlapVector(i);
}

void HirschLabeling::computeOverlapVector(int i)
{
    Label *l1 = labels[i];

    overlap_vectors[i][X] = overlap_vectors[i][Y] = 0.0;

    LabelPtrListIterator it(labels[i]->getNeighbours());
    while (it.hasNext())
    {
        Label *l2 =  it.next();

        if (l1->doesIntersect(l2))
        {
            QRectF r = l1->getRectangle().intersected(l2->getRectangle());
            double dx = r.width() / 2;
            double dy = r.height() / 2;

            if (l1->getNode().getY() < l2->getNode().getY()) //l1 is on top of l2
                dy = -dy;

            if (l1->getNode().getX() < l2->getNode().getX()) //l1 is on top of l2
                dx = -dx;

            overlap_vectors[i][X] += dx;
            overlap_vectors[i][Y] += dy;
        }
    }
}

bool HirschLabeling::canSlideHorizontal(const Label *l) const
{
    return (
                (l->getOffsetVertical() == 0.0 || l->getOffsetVertical() == l->getHeight())
                && qAbs(overlap_vectors[l->getIndex()][X]) > 0
            && ((overlap_vectors[l->getIndex()][X] > 0.0 && l->getOffsetHorizontal() > 0)
            || (overlap_vectors[l->getIndex()][X] < 0.0 && l->getOffsetHorizontal() < l->getWidth())));

}

bool HirschLabeling::canSlideVertical(const Label * ptr) const
{
    const Label& l = *ptr;
    return (
                (l.getOffsetHorizontal() == 0.0 || l.getOffsetHorizontal() == l.getWidth())
                && qAbs(overlap_vectors[l.getIndex()][Y]) > 0.0
            && ((overlap_vectors[l.getIndex()][Y] > 0.0 && l.getOffsetVertical() > 0.0)
            || (overlap_vectors[l.getIndex()][Y] < 0.0 && l.getOffsetVertical() < l.getHeight())));

}

void HirschLabeling::slideBy(Label* ptr, int direction, double value)
{
    Label &l = *ptr;
    double h_offset = l.getOffsetHorizontal();
    double v_offset = l.getOffsetVertical();
    int i_label = l.getIndex();

    value = qAbs(value);

    if (direction == SLIDE_HORIZONTAL)
    {
        if (overlap_vectors[i_label][X] > 0)
            h_offset = qMax(0., l.getOffsetHorizontal() - value);
        else
            h_offset = qMin(l.getWidth(), l.getOffsetHorizontal() + value);
    }
    else
    {
        if (overlap_vectors[i_label][Y] > 0)
            v_offset = qMax(0., l.getOffsetVertical() - value);
        else
            v_offset = qMin(l.getHeight(), l.getOffsetVertical() + value);
    }

    l.moveTo(h_offset, v_offset);
}

