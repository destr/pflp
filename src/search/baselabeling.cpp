#include "baselabeling.h"


BaseLabeling::BaseLabeling() : halt(false) {
}

BaseLabeling::~BaseLabeling() {

}

QString BaseLabeling::getAlgorithmName() const {
    if (name.isEmpty())
        return QString("<not set>");
    else
        return name;
}

void BaseLabeling::run() {

    halt = false;

    //execute precompute
    SolutionPtr s = DataSingleton::instance()->solution;

    precompute();

    //start doing the "real" work...
    while (!halt) {

        //gain access to current solution
        s = DataSingleton::instance()->solution;

        halt = iterate();
    }
}

QString BaseLabeling::getLabelInfo(int i) const {
    Q_UNUSED(i);
    return QString();
}

void BaseLabeling::cleanupSolution(SolutionPtr solution)
{
    LabelPtrList labels = solution->getLabels();
    int n = labels.size();
    int *numoverlaps = new int[n];

    int next_idx = -1;

    for(int i=0; i < n; i++)
    {
        Label *l = labels[i];
        numoverlaps[i] = 0;

        if(!l->getUnplacable())
        {
            LabelPtrListIterator it(l->getNeighbours());
            while (it.hasNext())
            {
                Label *l2 = it.next();
                if (!l2->getUnplacable() && labels[i]->doesIntersect(l2))
                    numoverlaps[i]++;
            }
        }

        if(numoverlaps[i] > 0 && (next_idx == -1 || numoverlaps[i] > numoverlaps[next_idx]))
            next_idx = i;
    }

    while(next_idx > -1)
    {
        //remove label next_idx
        LabelPtrListIterator it(labels[next_idx]->getNeighbours());
        while (it.hasNext())
        {
            Label *l2 = it.next();
            if (!l2->getUnplacable() && labels[next_idx]->doesIntersect(l2))
                numoverlaps[l2->getIndex()]--;
        }

        labels[next_idx]->setUnplacable(true);

        //find next victim
        next_idx = -1;
        for(int i=0; i < n; i++)
        {
            if(!labels[i]->getUnplacable() && numoverlaps[i] > 0 && (next_idx == -1 || numoverlaps[i] > numoverlaps[next_idx]))
                next_idx = i;
        }
    }
}

void BaseLabeling::cleanupSolutionSimple(SolutionPtr solution)
{
    //simple algorithm...
    LabelPtrList labels = solution->getLabels();
    int size = labels.size();
    Label *found(nullptr);
    int max_ovl = -1;

    do
    {
        found = nullptr;
        max_ovl = -1;
        for (int i = 0; i < size; i++)
        {
            if (labels[i]->getUnplacable())
                continue;

            int ovl_count = 0;
            LabelPtrListIterator it(labels[i]->getNeighbours());
            while (it.hasNext())
            {
                Label *l2 = it.next();
                if (!l2->getUnplacable() && labels[i]->doesIntersect(l2))
                    ovl_count++;
            }

            if (ovl_count > 0 && (found == nullptr || ovl_count > max_ovl))
            {
                max_ovl = ovl_count;
                found = labels[i];
            }
        }

        if (found != nullptr)
            found->setUnplacable(true);

    }
    while (found != nullptr);
}

