#pragma once

#include <QtWidgets/QWidget>

#include "label.h"

class Visualisation : public QWidget {
    Q_OBJECT
public:
    explicit Visualisation(QWidget *parent = 0);
    ~Visualisation();

    void setDrawEnabled(bool enabled);
    void setLabelList(const LabelPtrList& list);

    // QWidget interface
protected:
    void paintEvent(QPaintEvent*event) Q_DECL_OVERRIDE;

private:
    bool draw_enabled_;
    LabelPtrList list_;

private:
    Q_DISABLE_COPY(Visualisation)
};
