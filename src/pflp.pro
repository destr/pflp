TEMPLATE = app
TARGET = pflp
INCLUDEPATH += .
CONFIG += c++11 console

QT += widgets gui

DESTDIR=$$PWD/../bin/
# Input
SOURCES +=                              \
    datasingleton.cpp                   \
    instance.cpp                        \
    label.cpp                           \
    main.cpp                            \
    mainwindow.cpp                      \
    pflp.cpp                            \
    pointfeature.cpp                    \
    random.cpp                          \
    search/baselabeling.cpp             \
    search/forcedirectedlabeling.cpp    \
    search/hirschlabeling.cpp           \
    solution.cpp                        \
    util/semaphore.cpp                  \
    visualisation.cpp                   \


HEADERS +=                          \
    datasingleton.h                 \
    instance.h                      \
    label.h                         \
    mainwindow.h                    \
    pflp.h                          \
    pointfeature.h                  \
    random.h                        \
    search/baselabeling.h           \
    search/forcedirectedlabeling.h  \
    search/hirschlabeling.h         \
    solution.h                      \
    visualisation.h



FORMS +=        \
    pflp.ui



