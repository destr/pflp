#include <QtCore/QPointF>
#include <QtCore/QRectF>
#include <QtCore/QVector>
#include <QtCore/QtMath>

#include "label.h"

Label::Label() {

}

Label::Label(PointFeature n, int idx) : node(n) {

    index = idx;
    moveTo(getWidth() / 2, getHeight() / 2);
}

Label::Label(const Label& label) : node(label.getNode())
{
    lbl_h_offset = label.getOffsetHorizontal();
    lbl_v_offset = label.getOffsetVertical();
    unplacable = label.getUnplacable();
    index = label.getIndex();
    updateCacheTopLeftPoint();
}

Label::~Label() {

}

void Label::setUnplacable(bool b)
{
    unplacable = b;
}

bool Label::getUnplacable() const
{
    return unplacable;
}

bool Label::isOverlapping() const
{
    if (getUnplacable())
        return false;

    LabelPtrListIterator it(neighbours);
    while (it.hasNext())
    {
        Label *next = it.next();

        if (next->getUnplacable())
            continue;

        if (doesIntersect(next))
        {
            return true;
        }
    }

    return false;
}

bool Label::doesIntersect(const Label *l2) const
{
    QPointF tl1 = getTopleft();
    QPointF tl2 = l2->getTopleft();

    return (
                (tl2.x() + l2->getWidth() > tl1.x() && tl2.x() < tl1.x() + getWidth())
                && (tl2.y() + l2->getHeight() > tl1.y() && tl2.y() < tl1.y() + getHeight()));
}

bool Label::doesIntersectHorizontal(const Label *l2) const {
    QPointF tl1 = getTopleft();
    QPointF tl2 = l2->getTopleft();

    return ((tl2.x() + l2->getWidth() > tl1.x() && tl2.x() < tl1.x() + getWidth()));
}

bool Label::doesIntersectVertical(const Label *l2) const {
    QPointF tl1 = getTopleft();
    QPointF tl2 = l2->getTopleft();

    return ((tl2.y() + l2->getHeight() > tl1.y() && tl2.y() < tl1.y() + getHeight()));
}

void Label::findInitialPlacement() {
    std::uniform_int_distribution<> dist(0, 1);
    std::default_random_engine generator;
    //use randomly one of the four corners....
    moveTo(getWidth() * dist(generator), getHeight() * dist(generator));

}

void Label::moveTo(int pos)
{
    if(pos != TOPLEFT && pos != TOPRIGHT && pos != BOTTOMLEFT && pos != BOTTOMRIGHT)
        return;

    double f1 = (pos == TOPLEFT || pos == BOTTOMLEFT) ? 1 : 0;
    double f2 = (pos == TOPLEFT || pos == TOPRIGHT) ? 1: 0;

    moveTo(f1 * getWidth(), f2*getHeight());
}

void Label::moveTo(double h_offset, double v_offset) {
    lbl_h_offset = h_offset;
    lbl_v_offset = v_offset;

    updateCacheTopLeftPoint();
}

void Label::addNeighbour(Label* l)
{
    neighbours.append(l);
}

bool Label::hasNeighbours() const
{
    return !neighbours.isEmpty();
}

const LabelPtrList& Label::getNeighbours() const
{
    return neighbours;
}

const PointFeature& Label::getNode() const
{
    return node;
}

QPointF Label::getTopleft() const {
    return cache_topLeftPoint_;
}

double Label::getWidth() const
{
    return node.getWidth();
}

double Label::getHeight() const
{
    return node.getHeight();
}

double Label::getOffsetHorizontal() const
{
    return lbl_h_offset;
}

double Label::getOffsetVertical() const {
    return lbl_v_offset;
}

bool Label::isTopLeft() const {
    return getOffsetHorizontal() == getWidth() && getOffsetVertical() == getHeight();
}

bool Label::isTopRight() const
{
    return getOffsetHorizontal() == 0 && getOffsetVertical() == getHeight();
}

bool Label::isBottomLeft() const {
    return getOffsetHorizontal() == getWidth() && getOffsetVertical() == 0;
}

bool Label::isBottomRight() const {
    return getOffsetHorizontal() == 0 && getOffsetVertical() == 0;
}

QRectF Label::getRectangle() const
{
    QPointF tl = getTopleft();
    return QRectF(tl.x(), tl.y(), getWidth(), getHeight());
}

double Label::getDistance(const Label* l2) const
{
    QPointF tl1 = getTopleft();
    QPointF tl2 = l2->getTopleft();

    double d = 0.0;

    bool h_intersect = doesIntersectHorizontal(l2);
    bool v_intersect = doesIntersectVertical(l2);

    if (v_intersect && h_intersect)
    {
        d = -1.0;
    }
    else if (h_intersect)
    {
        d = qAbs(tl1.y() - tl2.y());
        if (tl1.y() < tl2.y())
            d -= getHeight();
        else
            d -= l2->getHeight();
    }
    else if (v_intersect)
    {
        d = qAbs(tl1.x() - tl2.x());
        if (tl1.x() < tl2.x())
            d -= getWidth();
        else
            d -= l2->getWidth();
    }
    else
    {
        double x1 = tl1.x();
        double y1 = tl1.y();
        double x2 = tl2.x();
        double y2 = tl2.y();

        if (tl1.x() > tl2.x())
        {
            if (tl1.y() > tl2.y())
            {
                y1 += getHeight();
                x2 += l2->getWidth();
            }
            else
            {
                x2 += l2->getWidth();
                y2 += l2->getHeight();
            }
        }
        else
        {
            if (tl1.y() > tl2.y())
            {
                x1 += getWidth();
                y1 += getHeight();
            }
            else
            {
                y2 += l2->getHeight();
                x1 += getWidth();
            }
        }

        double dx = x1 - x2;
        double dy = y1 - y2;

        d = qSqrt(dx * dx + dy * dy);
    }

    return d;
}

int Label::getIndex() const {
    return index;
}

void Label::updateCacheTopLeftPoint() {
    cache_topLeftPoint_ = QPointF(node.getX() - lbl_h_offset, node.getY() - lbl_v_offset);
}  // updateCacheTopLeftPoint

QPointF Label::getCenter() const {
    return QPointF(
                node.getX() - lbl_h_offset + (getWidth() / 2),
                node.getY() - lbl_v_offset + (getHeight() / 2));
}
