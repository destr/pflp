#pragma once

#include <QtCore/QPointF>
#include <memory>
#include "pointfeature.h"


class Label;

typedef QVector<Label*> LabelPtrList;

typedef QVectorIterator<Label*> LabelPtrListIterator;


/**
 * Represents a mapping of a label (specified by the corresponding node)
 * to (x,y) corrdinates.
 * @author Ebner Dietmar, ebner@apm.tuwien.ac.at
 */
class Label {
public:
    const static int TOPLEFT = 1;
    const static int TOPRIGHT = 2;
    const static int BOTTOMLEFT = 3;
    const static int BOTTOMRIGHT = 4;
	

public:
    Label();
	/**
	 * constructs a new label centerd at the specified city.
	 * @param n the corresponding {@link LblNode node}
	 */
    Label(PointFeature n, int idx);

	/**
	* cretes an exact copy of the given label
	* don't forget to add all neighbours to the new object
	* @param label
	*/
    Label(const Label &label);
    ~Label();

	/**
	 * Allows to mark the current label as "unset"
	 * @param b true, if the label shouldn't be included in the current solution
	 */
    void setUnplacable(bool b);

	/**
	 * Returns true, if the label is not included in the current solution (not labeled)
	 */
    bool getUnplacable() const;

	/**
	 * Returns true, if the label intersects with one of it's neighbours.
	 */
    bool isOverlapping() const;

	/**
	 * Returns true, if the current label intersects with the given
	 * label l2. This means, there is at least on point covered by
	 * both labels.
	 * @param l2 a reference to the label to compare
	 */
    bool doesIntersect(const Label* l2) const;

	/**
	 * Returns true, if the current label intersects the given label in horizontal direction.
	 * @param l2 a reference to the label to compare
	 */
    bool doesIntersectHorizontal(const Label* l2) const;

	/**
	 * Returns true, if the current label intersects the given label in vertical direction.
	 * @param l2 a reference to the label to compare
	 */
    bool doesIntersectVertical(const Label* l2) const;

	/**
	 * Searches a starting position for the given label.
	 */
    void findInitialPlacement();

    void moveTo(int pos);

	/**
	 * Moves the label to the position. Note, that the given values are not
	 * checked and should be calculated carefully.
	 * A offset of (0, 0) means the lower right corner.
	 * @param h_offset the horizontal offset
	 * @param v_offset the vertical offset
	 */
    void moveTo(double h_offset, double v_offset);

	/**
	 * Adds the given label to the Vector of neighbours.
	 * @param l a reference to a label, that can intersect with the current label in at least one possible position
	 */
    void addNeighbour(Label* l);

	/**
	 * Returns true, if there is at least one label that could intersect in
	 * any position with the current label. If has_neighbours() returns false,
	 * any given position is a valid labeling.
	 */
    bool hasNeighbours() const;

	/**
	 * Returns a Vector of the labels neighbours.
	 * @return a reference to a Vector of neighbours
	 */
    const LabelPtrList& getNeighbours() const;

	/**
	 * Returns a reference to the corresponding node.
	 */
    const PointFeature& getNode() const;

	/**
	 * Returns the center point of the current label.
	 */
    QPointF getCenter() const;

	/**
	 * Returns the top left point of the current label.	
	 */
    QPointF getTopleft() const;

	/**
	 * Returns the width of the current label.
	 */
    double getWidth() const;

	/**
	 * Returns the height of the current label.
	 */
    double getHeight() const;

	/**
	 * Returns the horizontal offset. Zero means the label is 
	 * positioned with it's left border at the city.
	 */
    double getOffsetHorizontal() const;

	/**
	 * Returns the vertical offset. Zero means the label is 
	 * positioned with it's top border at the city.
	 */
    double getOffsetVertical() const;

	/**
	 * @return true <-> label is placed at the upper left corner  
	 */
    bool isTopLeft() const;

	/**
	 * @return true <-> label is placed at the upper right corner  
	 */
    bool isTopRight() const;

	/**
	 * @return true <-> label is placed at the lower left corner  
	 */
    bool isBottomLeft() const;

	/**
	 * @return true <-> label is placed at the lower right corner  
	 */
    bool isBottomRight() const;

	/**
	 * Returns a rectangle representing the current placement.
	 */
    QRectF getRectangle() const;

	/**
	 * returns the smallest distance between two rectangles, -1 if they overlap...
	 */
    double getDistance(const Label* l2) const;
	
	/**
	 * @return index of the label in  the current instance
	 */
    int getIndex() const;

private:
    void updateCacheTopLeftPoint();

private:
    //a reference to the node itself
    const PointFeature node;

    //a reference to all neighbours
    LabelPtrList neighbours;

    //the position of the label
    double lbl_h_offset = 0.0; // 0 <= lbl_h_offset <= node.lbl_width
    double lbl_v_offset = 0.0; // 0 <= lbl_v_offset <= node.lbl_height

    //true <-> label is not included in the current solution
    bool unplacable = false;

    //index of the label in the solution object...
    int index = -1;

    // Точка
    QPointF cache_topLeftPoint_;
};


