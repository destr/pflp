#include "instance.h"



Instance::Instance(const PointFeatureList& v, QString solution_name)
{
    int i;
    nodes.reserve(v.size());
    for (i = 0; i < v.size(); i++)
        nodes[i] = v.at(i);

    qDebug(i + " labels processed...");
    name = solution_name;

    adjust_coordinates();
}

Instance::Instance()
{
    int i = 0;

    name = "random instance";

    map_width = RANDOM_MAP_WIDTH * RANDOM_MAP_SCALE;
    map_height = RANDOM_MAP_HEIGHT * RANDOM_MAP_SCALE;

    qDebug("creating random instance with %d nodes...", RANDOM_MAP_LABELS);


    typedef QVector<bool> BoolList;
    typedef QVector<BoolList> Bool2List;

    Bool2List taken(RANDOM_MAP_WIDTH, BoolList(RANDOM_MAP_HEIGHT, false));

    std::default_random_engine generator;
    std::uniform_int_distribution<> dis_width(0, RANDOM_MAP_WIDTH - 1);
    std::uniform_int_distribution<> dis_height(0, RANDOM_MAP_HEIGHT - 1);

    nodes.resize(RANDOM_MAP_LABELS);
    for (i = 0; i < RANDOM_MAP_LABELS; i++)
    {
        int x, y;
        do
        {
            x = dis_width(generator);
            y = dis_height(generator);
        }
        while (taken[x][y]);

        taken[x][y] = true;

        std::uniform_int_distribution<> dis_text(5, 12);
        std::uniform_int_distribution<> dis_letter(65, 91);
        //generate random text

        int text_length = dis_text(generator);
        QByteArray s(text_length, 0);
        for (; text_length-- > 0;)
            s[text_length] = dis_letter(generator);

        QString text(s.constData());

        //determine the type of the city (priority)
        int priority = 1;
        std::uniform_int_distribution<> dis_prior(1, 3);
        priority = dis_prior(generator);


        nodes[i] = PointFeature(x * RANDOM_MAP_SCALE, y * RANDOM_MAP_SCALE, priority, text);
    }

    qDebug("done");
    adjust_coordinates();
}

void Instance::adjust_coordinates()
{
    double min_x = std::numeric_limits<double>::max();
    double min_y = std::numeric_limits<double>::max();

    qDebug("adjusting coordinates...");
    for (int i = 0; i < nodes.size(); i++)
    {
        PointFeature c = nodes[i];
        if (c.getX() - c.getWidth() < min_x)
            min_x = c.getX() - c.getWidth();

        if (c.getY() - c.getHeight() < min_y)
            min_y = c.getY() - c.getHeight();
    }

    double h_offset = -min_x;
    double v_offset = -min_y;

    double max_x = std::numeric_limits<double>::min();
    double max_y = std::numeric_limits<double>::min();

    //add offset to each label...
    for (int i = 0; i < nodes.size(); i++)
    {
        PointFeature c = nodes[i];

        c.setX(c.getX() + h_offset);
        c.setY(c.getY() + v_offset);

        if (c.getX() + c.getWidth() > max_x)
            max_x = c.getX() + c.getWidth();

        if (c.getY() + c.getHeight() > max_y)
            max_y = c.getY() + c.getHeight();
    }

    map_width = (int)qCeil(max_x);
    map_height = (int)qCeil(max_y);

    qDebug("done");
}

int Instance::getMapWidth() const
{
    return map_width;
}

int Instance::getMapHeight() const
{
    return map_height;
}

const PointFeatureList& Instance::getNodes() const
{
    return nodes;
}

const QString&Instance::getName() const {
    return name;
}

void Instance::setName(QString string) {
    name = string;
}

int Instance::size() const {
    return nodes.size();
}

